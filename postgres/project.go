package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log/slog"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"

	"gitlab.com/semiconware/orange"
)

type ProjectService struct {
	db *sqlx.DB
}

func NewProjectService(db *sqlx.DB) *ProjectService {
	return &ProjectService{db: db}
}

// Initialize a default logger
var logger = slog.Default()

func (s *ProjectService) CreateProject(ctx context.Context, userID int, project *orange.Project) error {
	tx, err := s.db.BeginTxx(ctx, nil)
	if err != nil {
		return err
	}
	project.UserID = userID
	if err := createProject(ctx, tx, userID, project); err != nil {
		logger.Error("Failed to create project", "error", err)
		return err
	}
	if err := tx.Commit(); err != nil {
		return err
	}
	return nil
}

func (p *ProjectService) FindProject(ctx context.Context, filter orange.ProjectFilter) ([]*orange.Project, int, error) {
	tx, err := p.db.BeginTxx(ctx, nil)
	if err != nil {
		return nil, 0, err
	}
	defer tx.Rollback()

	// Fetch list of matching dial objects.
	projects, n, err := findProject(ctx, tx, filter)
	if err != nil {
		return projects, n, err
	}
	// // Attach project associations if necessary
	// if err := s.attachProjectAssociations(updatedProject); err != nil {
	// 	return updatedProject, err
	// }

	return projects, n, err
}

// func (ps *ProjectService) FindProjectById(ctx context.Context, id uint) (*orange.Project, error) {
// 	tx, err := ps.db.BeginTxx(ctx, nil)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer tx.Rollback()

// 	// Fetch dial object and attach owner user.
// 	project, err := findProjectById(id, tx)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return project, nil
// }

func (p *ProjectService) FindProjectById(ctx context.Context, id uint) (*orange.Project, error) {
	// Begin a transaction using sqlx
	tx, err := p.db.BeginTxx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	// Fetch project object.
	project, err := findProjectById(ctx, tx, id)
	if err != nil {
		return nil, err
	}

	// Commit the transaction if everything is fine
	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return project, nil
}

func findProjectById(ctx context.Context, tx *sqlx.Tx, id uint) (*orange.Project, error) {
	var project orange.Project
	query := "SELECT * FROM projects WHERE id = :id"
	namedStmt, err := tx.PrepareNamedContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer namedStmt.Close()

	err = namedStmt.GetContext(ctx, &project, map[string]interface{}{"id": id})
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("project with ID %d not found", id)
		}
		return nil, err
	}

	return &project, nil
}
func (s *ProjectService) UpdateProject(ctx context.Context, userID int, id uint, project *orange.Project) (*orange.Project, error) {
	// Begin a new transaction
	tx, err := s.db.BeginTxx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer func() {
		// Rollback the transaction if there's an error and it hasn't been committed
		if err != nil {
			tx.Rollback()
		}
	}()

	// Update the project details
	project.UserID = userID
	updatedProject, err := updateProject(ctx, s, tx, userID, id, project)
	if err != nil {
		logger.Error("Failed to update project", "error", err)
		return nil, err
	}

	// Commit the transaction
	if err := tx.Commit(); err != nil {
		logger.Error("Failed to commit transaction", "error", err)
		return nil, err
	}

	// // Attach project associations if necessary
	// if err := s.attachProjectAssociations(updatedProject); err != nil {
	// 	return updatedProject, err
	// }

	return updatedProject, nil
}

func (s *ProjectService) DeleteProject(ctx context.Context, userID int, id uint) error {
	// Begin a new transaction
	tx, err := s.db.BeginTxx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		// Rollback the transaction if there's an error and it hasn't been committed
		if err != nil {
			tx.Rollback()
		}
	}()

	// Find the project by its ID
	project, err := s.FindProjectById(ctx, id)
	if err != nil {
		return err
	}

	// Check if the user is authorized to delete the project
	if project.UserID != userID {
		return errors.New("unauthorized: user does not own this project")
	}

	// Delete the project
	err = deleteProject(ctx, s, tx, id)
	if err != nil {
		logger.Error("Failed to delete project", "error", err)
		return err
	}

	// Commit the transaction
	if err := tx.Commit(); err != nil {
		logger.Error("Failed to commit transaction", "error", err)
		return err
	}
	return nil
}

func createProject(ctx context.Context, tx *sqlx.Tx, userID int, project *orange.Project) error {
	project.UserID = userID
	query := `
        INSERT INTO projects (title, completed_at, deleted_at, user_id)
        VALUES (:title, :completed_at, :deleted_at, :user_id)
        RETURNING id
    `
	stmt, err := tx.PrepareNamedContext(ctx, query)
	if err != nil {
		return fmt.Errorf("error preparing query: %w", err)
	}
	defer stmt.Close()

	err = stmt.QueryRowxContext(ctx, project).Scan(&project.ID)
	if err != nil {
		return fmt.Errorf("error creating project: %w", err)
	}
	return nil
}
func findProject(ctx context.Context, tx *sqlx.Tx, filter orange.ProjectFilter) ([]*orange.Project, int, error) {
	// Initialize the where clause and parameters map.
	where := []string{}
	params := map[string]interface{}{}

	// Add filtering based on the provided filter object.
	if v := filter.ID; v != nil {
		where = append(where, "id = :id")
		params["id"] = *v
	}

	if v := filter.Title; v != nil {
		where = append(where, "to_tsvector('english', title) @@ to_tsquery('english', :title)")
		params["title"] = *v
	}

	// Add pagination parameters to the query.
	limitOffset := ""
	if filter.Limit != 0 {
		limitOffset += " LIMIT :limit"
		params["limit"] = filter.Limit
	}
	if filter.Offset != 0 {
		limitOffset += " OFFSET :offset"
		params["offset"] = filter.Offset
	}

	// Add sorting parameters to the query.
	sortBy := "id" // Default sort by ID
	if filter.SortBy != "" {
		sortBy = filter.SortBy
	}

	sortOrder := "ASC" // Default sort order
	if filter.SortOrder != "" {
		sortOrder = filter.SortOrder
	}

	// Build the SELECT query with the WHERE clause, sorting, and pagination.
	query := `
        SELECT
            id, title, completed_at, deleted_at, created_at, updated_at,
            COUNT(*) OVER() AS total_count
        FROM projects`

	// Append the WHERE clause if there are conditions.
	if len(where) > 0 {
		query += " WHERE " + strings.Join(where, " AND ")
	}

	// Append the ORDER BY and LIMIT/OFFSET clauses.
	query += `
        ORDER BY ` + sortBy + ` ` + sortOrder + `
        ` + limitOffset

	// Prepare the named query with parameters.
	namedQuery, args, err := sqlx.Named(query, params)
	if err != nil {
		return nil, 0, err
	}

	// Rebind the named query for the specific database driver.
	namedQuery = tx.Rebind(namedQuery)

	// Execute the query.
	rows, err := tx.QueryContext(ctx, namedQuery, args...)
	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	// Iterate over rows and deserialize into Project objects.
	projects := make([]*orange.Project, 0)
	var totalCount int
	for rows.Next() {
		var project orange.Project
		if err := rows.Scan(
			&project.ID,
			&project.Title,
			&project.CompletedAt,
			&project.DeletedAt,
			&project.CreatedAt,
			&project.UpdatedAt,
			&totalCount,
		); err != nil {
			return nil, 0, err
		}
		projects = append(projects, &project)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return projects, totalCount, nil
}

// func findProject(ctx context.Context, filter orange.ProjectFilter, tx *sqlx.Tx) ([]*orange.Project, int, error) {
// 	// Build WHERE clause. Each part of the WHERE clause is AND-ed together.
// 	where := []string{"1=1"}
// 	params := map[string]interface{}{}

// 	// Add filtering based on the provided filter object.
// 	if v := filter.ID; v != nil {
// 		where = append(where, "id = :id")
// 		params["id"] = *v
// 	}

// 	// Add full-text search condition if searchText is provided.
// 	if v := filter.Title; v != nil {
// 		where = append(where, "to_tsvector('english', title) @@ plainto_tsquery('english', :title)")
// 		params["title"] = *v
// 	}

// 	sortField := "id"
// 	if filter.SortField == "" {
// 		sortField = filter.SortField
// 	}
// 	sortOrder := "ASC"
// 	if filter.SortOrder != "" {
// 		sortOrder = filter.SortOrder
// 	}

// 	// Execute the query.
// 	query := `
//        SELECT
//            id, title, deleted_at, created_at, updated_at, completed_at,user_id
//        FROM projects
//        WHERE ` + strings.Join(where, " AND ")+`
// 	   ORDER BY`+sortField+` `+sortOrder+`
// 	   `+FormatLimitOffset(filter.Limit,filter.Offset),
// 	   args...

// 	// Prepare the named query with parameters.
// 	namedQuery, args, err := sqlx.Named(query, params)
// 	if err != nil {
// 		return nil, 0, err
// 	}

// 	// Rebind the named query for the specific database driver.
// 	namedQuery = tx.Rebind(namedQuery)

// 	// Execute the query.
// 	rows, err := tx.Query(namedQuery, args...)
// 	if err != nil {
// 		return nil, 0, err
// 	}
// 	defer rows.Close()

// 	// Iterate over rows and deserialize into Project objects.
// 	projects := make([]*orange.Project, 0)
// 	for rows.Next() {
// 		var project orange.Project
// 		if err := rows.Scan(
// 			&project.ID,
// 			&project.Title,
// 			&project.DeletedAt,
// 			&project.CreatedAt,
// 			&project.UpdatedAt,
// 			&project.CompletedAt,
// 			&project.UserID,
// 		); err != nil {
// 			return nil, 0, err
// 		}
// 		projects = append(projects, &project)
// 	}
// 	if err := rows.Err(); err != nil {
// 		return nil, 0, err
// 	}

// 	return projects, len(projects), nil
// }

// func findProjectById(id uint, tx *sqlx.Tx) (*orange.Project, error) {
// 	var project orange.Project
// 	query := "SELECT * FROM projects WHERE id = :id"
// 	rows, err := tx.NamedQuery(query, map[string]interface{}{"id": id})
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer rows.Close()

// 	if rows.Next() {
// 		if err := rows.StructScan(&project); err != nil {
// 			return nil, err
// 		}
// 	}

// 	return &project, nil
// }

// func updateProject(db *sqlx.DB, id uint, project *orange.Project) (*orange.Project, error) {

// 	currentTime := time.Now()
// 	query := `
//         UPDATE projects
//         SET title = :title, completed_at = :completed_at, deleted_at = :deleted_at, updated_at = :updated_at
//         WHERE id = :id `
// 	logger.Debug("Executing SQL Query", "query", query)
// 	_, err := db.NamedExec(query, map[string]interface{}{
// 		"title":        project.Title,
// 		"completed_at": project.CompletedAt,
// 		"deleted_at":   project.DeletedAt,
// 		"updated_at":   currentTime,
// 		"id":           id,
// 	})

//		if err != nil {
//			return nil, err
//		}
//		project.UpdatedAt = currentTime
//		// Return the updated project
//		return project, nil
//	}
func updateProject(ctx context.Context, p *ProjectService, tx *sqlx.Tx, userID int, id uint, project *orange.Project) (*orange.Project, error) {
	projectType, err := p.FindProjectById(ctx, id)
	if err != nil {
		return nil, err
	}

	if projectType.UserID != userID {
		return nil, errors.New("unauthorized: user does not own this project")
	}

	var update []string
	updated := make(map[string]interface{})

	if project.Title != "" {
		updated["title"] = project.Title
		projectType.Title = project.Title
	}
	if !project.CompletedAt.IsZero() {
		updated["completed_at"] = project.CompletedAt
		projectType.CompletedAt = project.CompletedAt
	}

	for k := range updated {
		update = append(update, fmt.Sprintf("%s = :%s", k, k))
	}

	projectType.UpdatedAt = time.Now()
	updated["updated_at"] = projectType.UpdatedAt

	updated["id"] = id

	query := fmt.Sprintf("UPDATE projects SET %s WHERE id = :id", strings.Join(update, ", "))

	_, err = tx.NamedExecContext(ctx, query, updated)
	if err != nil {
		return nil, err
	}
	return projectType, nil
}

func deleteProject(ctx context.Context, p *ProjectService, tx *sqlx.Tx, id uint) error {
	// Execute the delete query
	query := "DELETE FROM projects WHERE id = :id"
	_, err := tx.NamedExecContext(ctx, query, map[string]interface{}{"id": id})
	if err != nil {
		return err
	}
	return nil
}

// func (s *ProjectService) attachProjectAssociations(project *orange.Project) error {
// 	// projectID := uint64(task.ProjectID)
// 	projects, err := s.FindUserById(project.UserID)
// 	if err != nil {
// 		return fmt.Errorf("attach task project: %w", err)
// 	}
// 	project.User = projects
// 	return nil
// }
