package postgres

import (
	"fmt"
	"log"

	_ "github.com/jackc/pgx/v5"
	"github.com/jmoiron/sqlx"
	"gitlab.com/semiconware/orange"
)

func InitDB() *sqlx.DB {
	db, err := sqlx.Open("pgx", "host=localhost user=b_pradnya password='Knight@123' dbname=orangedb port=5432 sslmode=disable")
	if err != nil {
		log.Fatal("Error connecting to the database:", err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal("Connection error:", err)
	}

	fmt.Println("Connected to the DB Successfully")
	return db
}

type DB struct {
	db *sqlx.DB
}

func NewDB(db *sqlx.DB) *DB {
	return &DB{db: db}
}

func FormatLimitOffset(limit, offset int) string {
	if limit > 0 && offset > 0 {
		return fmt.Sprintf(`LIMIT %d OFFSET %d`, limit, offset)
	} else if limit > 0 {
		return fmt.Sprintf(`LIMIT %d`, limit)
	} else if offset > 0 {
		return fmt.Sprintf(`OFFSET %d`, offset)
	}
	return ""
}
func FormatError(err error) error {
	if err == nil {
		return nil
	}

	switch err.Error() {
	case "UNIQUE constraint failed: dial_memberships.dial_id, dial_memberships.user_id":
		return orange.Errorf(orange.ECONFLICT, "Dial membership already exists.")
	default:
		return err
	}
}
