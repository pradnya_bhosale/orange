package postgres

import (
	"fmt"

	"github.com/jmoiron/sqlx"

	"gitlab.com/semiconware/orange"
)

type UserService struct {
	db *sqlx.DB
}

func NewUserService(db *sqlx.DB) *UserService {
	return &UserService{db: db}
}
func (s *UserService) RegisterUser(user *orange.User) error {

	// Hash the password
	if err := user.HashPassword(); err != nil {
		return err
	}

	if err := registerUser(s.db, user); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

// func (s *UserService) LoginUser(user *orange.User) error {
// 	dbUser, err := loginUser(s.db, user)
// 	if err != nil {
// 		return err
// 	}

// 	// Check if passwords match
// 	if dbUser.Password != user.Password {
// 		return fmt.Errorf("invalid email or password")
// 	}

// 	// Copy dbUser's data to user
// 	*user = dbUser

// 	return nil
// }

// Assume loginUser and other necessary functions are implemented

func (s *UserService) LoginUser(user *orange.User) (string, error) {
	// Retrieve user from the database
	dbUser, err := loginUser(s.db, user)
	if err != nil {
		return "", err
	}

	// Check if passwords match
	if !dbUser.CheckPassword(user.Password) {
		return "", fmt.Errorf("username email or password")
	}

	// Generate JWT token
	token, err := orange.GenerateJWT(dbUser.ID) // Pass pointer to dbUser
	//fmt.Println("Receivedtoken:", token)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (s *UserService) ValidateToken(token string) (int, error) {
	return orange.ValidateJWT(token)
}

func registerUser(db *sqlx.DB, user *orange.User) error {
	query := "INSERT INTO users (fullname, email, username, password, isactive, deleted_at) VALUES(:fullname, :email, :username, :password, :isactive, :deleted_at) RETURNING id, fullname, email, username, password, isactive, deleted_at"
	rows, err := db.NamedQuery(query, user)
	if err != nil {
		return err
	}
	defer rows.Close()

	if rows.Next() {
		err = rows.StructScan(user)
		if err != nil {
			return err
		}
	}

	return nil
}
func loginUser(db *sqlx.DB, user *orange.User) (orange.User, error) {
	var dbUser orange.User
	// Assuming db is your sqlx.DB instance
	err := db.Get(&dbUser, "SELECT id, fullname, email, username, password, isactive, created_at, updated_at, deleted_at FROM users WHERE username = $1", user.Username)
	if err != nil {
		return orange.User{}, fmt.Errorf("username not found or invalid password")
	}
	return dbUser, nil
}

func GetUserByEmail(email string) (*orange.User, error) {
	var db *sqlx.DB
	var user orange.User
	err := db.Get(&user, "SELECT email, password FROM users WHERE email = ?", email)

	if err != nil {
		return nil, fmt.Errorf("error fetching user by email: %v", err)
	}
	return &user, nil
}
func (s *ProjectService) FindUserById(userID int) (*orange.User, error) {
	var user orange.User
	err := s.db.Get(&user, "SELECT id, fullname, email, username, password, isactive, created_at, updated_at, deleted_at FROM users WHERE id = $1", userID)
	if err != nil {
		return nil, fmt.Errorf("find user by ID: %w", err)
	}
	return &user, nil
}
