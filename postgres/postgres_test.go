package postgres_test

import (
	"log"
	"testing"

	_ "github.com/jackc/pgx/v5"
	"github.com/jmoiron/sqlx"

	"gitlab.com/semiconware/orange"
	"gitlab.com/semiconware/orange/postgres"
)

func TestDB(t *testing.T) {
	db := testOpenInitDB(t)
	testCloseInitDB(t, db)
}

func testOpenInitDB(tb testing.TB) *sqlx.DB {
	tb.Helper()
	// Set up a test database connection
	db, err := sqlx.Open("pgx", "host=localhost user=b_pradnya password='Knight@123' dbname=orangedbtest port=5432 sslmode=disable")
	if err != nil {
		tb.Fatalf("Error connecting to the database: %v", err)
	}

	// Call the function under test
	dbReturned := postgres.InitDB()
	if err := orange.CreateUserTable(db); err != nil {
		defer db.Close()
		log.Fatal("Error creating project table:", err)
	}
	if err := orange.CreateProjectTable(db); err != nil {
		defer db.Close()
		log.Fatal("Error creating project table:", err)
	}

	// Check if the database connection is valid
	err = dbReturned.Ping()
	if err != nil {
		tb.Fatalf("Error pinging database: %v", err)
	}

	return db
}

func testCloseInitDB(tb testing.TB, db *sqlx.DB) {
	tb.Helper()
	if err := db.Close(); err != nil {
		tb.Fatal(err)
	}
}
