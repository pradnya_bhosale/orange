package postgres_test

import (
	"context"
	"testing"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/semiconware/orange"
	"gitlab.com/semiconware/orange/postgres"
)

func TestProjectService_CreateProject(t *testing.T) {
	// Initialize test database
	db := testOpenInitDB(t)
	defer testCloseInitDB(t, db)

	// Initialize user service and project service
	userService := postgres.NewUserService(db)
	projectService := postgres.NewProjectService(db)

	// Context for the test
	ctx := context.Background()

	// Register a new user
	newUser := &orange.User{
		FullName:  "Test User",
		Email:     "test@example.com",
		Username:  "testuser",
		Password:  "password",
		IsActive:  true,
		CreatedAt: time.Now(),
	}
	if err := userService.RegisterUser(newUser); err != nil {
		t.Fatalf("failed to register user: %v", err)
	}
	defer func() {
		// Clean up the registered user
		// Note: Implement a function to delete the user if needed
	}()

	t.Run("OK", func(t *testing.T) {
		// Login the registered user to obtain a JWT token
		token, err := userService.LoginUser(&orange.User{
			Username: "testuser",
			Password: "password",
		})
		if err != nil {
			t.Fatalf("failed to login user: %v", err)
		}

		// Validate the JWT token
		userID, err := userService.ValidateToken(token)
		if err != nil {
			t.Fatalf("failed to validate token: %v", err)
		}

		// Now userID contains the ID of the logged-in user
		project := &orange.Project{Title: "Student management System", CompletedAt: time.Now(), UserID: userID}

		// Create the project
		if err := projectService.CreateProject(ctx, project.UserID, project); err != nil {
			t.Fatalf("failed to create project: %v", err)
		}
		defer func() {
			// Clean up the created project
			if err := projectService.DeleteProject(ctx, project.UserID, project.ID); err != nil {
				t.Errorf("failed to delete project: %v", err)
			}
		}()

		// Check if the project exists
		foundProject, err := projectService.FindProjectById(ctx, project.ID)
		if err != nil {
			t.Fatalf("unexpected error: %#v", err)
		}
		if foundProject == nil {
			t.Fatal("expected project to exist, but got nil")
		}
	})
}

func TestProjectService_FindProject(t *testing.T) {
	t.Run("Owned", func(t *testing.T) {
		// Initialize test environment.
		db := testOpenInitDB(t)
		defer testCloseInitDB(t, db)
		// Context for the test
		ctx := context.Background()

		// Initialize user service
		userService := postgres.NewUserService(db)

		// Create a sample user
		user := &orange.User{
			FullName:  "Test User",
			Email:     "test@example.com",
			Username:  "testuser",
			Password:  "password",
			IsActive:  true,
			CreatedAt: time.Now(),
		}
		if err := userService.RegisterUser(user); err != nil {
			t.Fatalf("failed to register user: %v", err)
		}

		// Create a sample project with the user's ID
		project := &orange.Project{
			Title:       "LabManagementSystem",
			CompletedAt: time.Now(),
			DeletedAt:   time.Now(),
			UserID:      user.ID,
		}
		MustCreateProject(t, ctx, db, project)
		filter := orange.ProjectFilter{}
		s := postgres.NewProjectService(db)
		projects, count, err := s.FindProject(ctx, filter)
		if err != nil {
			t.Fatalf("Error finding projects: %v", err)
		}

		// Add assertions
		if count != 1 {
			t.Errorf("Expected count to be 1, but got %d", count)
		}
		if len(projects) != 1 {
			t.Errorf("Expected to find 1 project, but found %d", len(projects))
		} else {
			foundProject := projects[0]
			if foundProject.Title != project.Title {
				t.Errorf("Expected project title to be %q, but got %q", project.Title, foundProject.Title)
			}
		}
	})
}

func TestProjectService_DeleteProject(t *testing.T) {

	t.Run("OK", func(t *testing.T) {
		db := testOpenInitDB(t)
		defer testCloseInitDB(t, db)
		userService := postgres.NewUserService(db)
		s := postgres.NewProjectService(db)
		// Register a new user
		newUser := &orange.User{
			FullName:  "Test User",
			Email:     "test@example.com",
			Username:  "testuser",
			Password:  "password",
			IsActive:  true,
			CreatedAt: time.Now(),
		}
		if err := userService.RegisterUser(newUser); err != nil {
			t.Fatalf("failed to register user: %v", err)
		}
		defer func() {
			// Clean up the registered user
			// Note: Implement a function to delete the user if needed
		}()
		// Login the registered user to obtain a JWT token
		token, err := userService.LoginUser(&orange.User{
			Username: "testuser",
			Password: "password",
		})
		if err != nil {
			t.Fatalf("failed to login user: %v", err)
		}
		// Validate the JWT token
		userID, err := userService.ValidateToken(token)
		if err != nil {
			t.Fatalf("failed to validate token: %v", err)
		}

		// Context for the test
		ctx := context.Background()
		project := &orange.Project{Title: "Student management System", CompletedAt: time.Now(), UserID: userID}
		MustCreateProject(t, ctx, db, project)

		if err := s.DeleteProject(ctx, project.UserID, project.ID); err != nil {
			t.Fatal(err)
		}
		if _, err := s.FindProjectById(ctx, project.ID); err != nil {
			// if orange.ErrorCode(err) != orange.ENOTFOUND {
			// 	t.Fatalf("unexpected error: %#v", err)
			// }
		}
	})
}

func TestProjectService_UpdateProject(t *testing.T) {
	// Ensure a project title can be updated.
	t.Run("OK", func(t *testing.T) {

		// Initialize and close the test database.
		db := testOpenInitDB(t)
		defer testCloseInitDB(t, db)

		// Create a new project service with the test database.
		s := postgres.NewProjectService(db)
		userService := postgres.NewUserService(db)
		// Create a new user for the test.
		user := &orange.User{
			FullName:  "Test User",
			Email:     "test@example.com",
			Username:  "testuser",
			Password:  "password",
			IsActive:  true,
			CreatedAt: time.Now(),
		}
		if err := userService.RegisterUser(user); err != nil {
			t.Fatalf("failed to register user: %v", err)
		}
		defer func() {
			// Clean up the registered user
			// Note: Implement a function to delete the user if needed
		}()
		defer func() {
			// Clean up the registered user
			// Note: Implement a function to delete the user if needed
		}()
		// Login the registered user to obtain a JWT token
		token, err := userService.LoginUser(&orange.User{
			Username: "testuser",
			Password: "password",
		})
		if err != nil {
			t.Fatalf("failed to login user: %v", err)
		}
		// Validate the JWT token
		userID, err := userService.ValidateToken(token)
		if err != nil {
			t.Fatalf("failed to validate token: %v", err)
		}
		// Context for the test
		ctx := context.Background()
		// Create a sample project with the user's ID
		project := &orange.Project{
			Title:       "LabManagementSystem",
			CompletedAt: time.Now(),
			DeletedAt:   time.Now(),
			UserID:      userID,
		}
		MustCreateProject(t, ctx, db, project)

		// Define the updated title for the project.
		updatedTitle := "Updated Project Title"

		// Call the UpdateProject method to update the project title.
		updatedProject, err := s.UpdateProject(context.Background(), project.UserID, project.ID, &orange.Project{Title: updatedTitle})
		if err != nil {
			t.Fatalf("failed to update project: %v", err)
		}

		// Verify that the project title was updated successfully.
		if updatedProject.Title != updatedTitle {
			t.Fatalf("expected project title to be %q, but got %q", updatedTitle, updatedProject.Title)
		}

		// Fetch the updated project from the database by its ID.
		fetchedProject, err := s.FindProjectById(context.Background(), project.ID)
		if err != nil {
			t.Fatalf("failed to fetch updated project: %v", err)
		}

		// Ensure that the project title is updated in the database.
		if fetchedProject.Title != updatedTitle {
			t.Fatalf("expected project title in the database to be %q, but got %q", updatedTitle, fetchedProject.Title)
		}

	})
}

func MustCreateProject(tb testing.TB, ctx context.Context, db *sqlx.DB, project *orange.Project) *orange.Project {
	tb.Helper()
	if err := postgres.NewProjectService(db).CreateProject(ctx, project.UserID, project); err != nil {
		tb.Fatal(err)
	}
	return project
}
