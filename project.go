package orange

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
)

type Project struct {
	ID    uint   `json:"id" db:"id"`
	Title string `json:"title" db:"title" validate:"required,min=3,max=100"`

	CompletedAt time.Time `json:"completedAt" db:"completed_at" validate:"required"`
	CreatedAt   time.Time `json:"createdAt" db:"created_at" validate:"-"`
	UpdatedAt   time.Time `json:"updatedAt" db:"updated_at" validate:"-"`
	DeletedAt   time.Time `json:"deletedAt" db:"deleted_at" validate:"required"`
	UserID      int       `json:"user_id" db:"user_id"  validate:"-"`
	User        *User     `json:"user"`
}

func CreateProjectTable(db *sqlx.DB) error {
	_, err := db.Exec(`
	CREATE TABLE IF NOT EXISTS projects (
		id SERIAL PRIMARY KEY,
		title VARCHAR(255),
		completed_at TIMESTAMP,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		deleted_at TIMESTAMP,
		user_id INT NOT NULL,
		FOREIGN KEY (user_id) REFERENCES users(id) on DELETE CASCADE
	)
`)
	if err != nil {
		return err
	}

	// Check if the index already exists
	var exists bool
	err = db.QueryRow(`
        SELECT EXISTS (
            SELECT 1 FROM pg_indexes
            WHERE schemaname = 'public' AND tablename = 'projects' AND indexname = 'idx_projects_title_tsvector'
        )
    `).Scan(&exists)
	if err != nil {
		return err
	}

	// Create the index only if it doesn't exist
	if !exists {
		_, err = db.Exec(`
            CREATE INDEX idx_projects_title_tsvector ON projects USING GIN (to_tsvector('english', title))
        `)
		if err != nil {
			return err
		}
	}

	return nil
}

type Tx struct {
	*sqlx.Tx
}
type ProjectFilter struct {
	ID        *uint   `json:"id" db:"id"`
	Title     *string `json:"title" db:"title"`
	UserID    int     `json:"user_id" db:"user_id"  validate:"-"`
	Offset    uint
	Limit     uint
	SortBy    string
	SortOrder string
}

type ProjectService interface {
	FindProjectById(ctx context.Context, id uint) (*Project, error)
	//FindProject(project *[]Project) error
	FindProject(ctx context.Context, filter ProjectFilter) ([]*Project, int, error)

	CreateProject(ctx context.Context, userID int, project *Project) error
	UpdateProject(ctx context.Context, userID int, id uint, project *Project) (*Project, error)
	//UpdateProject(userID, projectID uint, project *Project) (*Project, error)
	DeleteProject(ctx context.Context, userID int, id uint) error
}
