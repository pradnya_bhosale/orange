package http

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"gitlab.com/semiconware/orange"
	"gitlab.com/semiconware/orange/middleware"
)

func (s *Server) projectRoutes() {

	s.Router.HandleFunc("POST /api/v1/projects", func(w http.ResponseWriter, r *http.Request) {
		handler := middleware.AuthMiddleware(s.UserService)(http.HandlerFunc(s.HandleCreateProjects))
		handler.ServeHTTP(w, r)
	})

	s.Router.HandleFunc("GET /api/v1/projects", s.HandleFindProject)
	s.Router.HandleFunc("GET /api/v1/projects/{id}", s.HandleFindProjectById)

	s.Router.HandleFunc("PUT /api/v1/projects/{id}", func(w http.ResponseWriter, r *http.Request) {
		handler := middleware.AuthMiddleware(s.UserService)(http.HandlerFunc(s.HandleUpdateProject))
		handler.ServeHTTP(w, r)
	})
	//s.Router.HandleFunc("DELETE /api/v1/projects/{id}", s.handleDeleteProject)
	s.Router.HandleFunc("DELETE /api/v1/projects/{id}", func(w http.ResponseWriter, r *http.Request) {
		handler := middleware.AuthMiddleware(s.UserService)(http.HandlerFunc(s.HandleDeleteProject))
		handler.ServeHTTP(w, r)
	})
}
func init() {
	validate = validator.New()
}
func (a *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.Router.ServeHTTP(w, r)
}

// ProjectService implements the wtf.DialService over the HTTP protocol.
type ProjectService struct {
	Client *Client
}

// NewProjectService returns a new instance of DialService.
func NewProjectService(client *Client) *ProjectService {
	return &ProjectService{Client: client}
}

// handleCreateProjects handles the creation of projects.
//
//	@Summary		Create Project
//	@Description	Create a new project.
//	@Tags			projects
//	@Accept			json
//	@Produce		json
//	@Param			project	body		orange.Project	true	"Project object to be created"
//	@Success		200		{object}	orange.Project
//	@Failure		400		{object}	orange.ErrorResponse
//	@Failure		404		{object}	orange.ErrorResponse
//	@Failure		500		{object}	orange.ErrorResponse
//	@Router			/projects [post]
func (s *Server) HandleCreateProjects(w http.ResponseWriter, r *http.Request) {
	var project orange.Project
	err := json.NewDecoder(r.Body).Decode(&project)
	if err != nil {
		orange.NewError(w, http.StatusBadRequest, err)
		return
	}
	defer r.Body.Close()

	if s.DB == nil {
		orange.NewError(w, http.StatusInternalServerError, errors.New("database connection not available"))
		return
	}

	// Validate the project struct
	validationErrors := validateStruct(&project)
	if validationErrors != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(validationErrors)
		return
	}
	userID := r.Context().Value("userID").(int)

	err = s.ProjectService.CreateProject(r.Context(), userID, &project)
	if err != nil {
		orange.NewError(w, http.StatusInternalServerError, err)
		return
	}

	//Return the projects as JSON in the response
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(project)

}

// handleFindProject Find project
//
//	@Summary		Get Projects
//	@Description	Get projects
//	@Tags			projects
//	@Accept			json
//	@Produce		json
//	@Param			q	query		string			false	"name search by q"
//	@Success		200	{array}		orange.ProjectFilter	"Successful response"
//	@Failure		400	{object}	orange.ErrorResponse
//	@Failure		404	{object}	orange.ErrorResponse
//	@Failure		500	{object}	orange.ErrorResponse
//	@Router			/projects [get]
func (s *Server) HandleFindProject(w http.ResponseWriter, r *http.Request) {
	var filter orange.ProjectFilter
	if id := r.URL.Query().Get("id"); id != "" {
		idUint, err := strconv.ParseUint(id, 10, 32)
		if err != nil {
			orange.NewError(w, http.StatusBadRequest, err)
			return
		}
		idUint32 := uint(idUint)
		filter.ID = &idUint32
	}
	if title := r.URL.Query().Get("title"); title != "" {
		filter.Title = &title
	}

	// Parse pagination parameters
	limit, err := strconv.ParseUint(r.URL.Query().Get("limit"), 10, 64)
	if err != nil {
		limit = 10 // Default limit
	}
	offset, err := strconv.ParseUint(r.URL.Query().Get("offset"), 10, 64)
	if err != nil {
		offset = 0 // Default offset
	}
	filter.Limit = uint(limit)
	filter.Offset = uint(offset)

	// Parse sorting parameters
	sortBy := r.URL.Query().Get("sortBy")
	if sortBy == "" {
		sortBy = "id" // Default sort by ID
	}
	filter.SortBy = sortBy

	sortOrder := r.URL.Query().Get("sortOrder")
	if sortOrder == "" {
		sortOrder = "ASC" // Default sort order
	}
	filter.SortOrder = sortOrder

	ctx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
	defer cancel()

	// Retrieve projects based on the filter, pagination, and sorting
	projects, totalCount, err := s.ProjectService.FindProject(ctx, filter)
	if err != nil {
		orange.NewError(w, http.StatusInternalServerError, err)
		return
	}

	// Encode response with projects and pagination metadata
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(map[string]interface{}{
		"projects":   projects,
		"totalCount": totalCount,
		"limit":      filter.Limit,
		"offset":     filter.Offset,
	}); err != nil {
		orange.NewError(w, http.StatusInternalServerError, err)
		return
	}
}

// handleFindProjectById FindProjectById
//
//	@Summary		Get Projects
//	@Description	get by id projects
//	@Tags			projects
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int						true	"Project ID"
//	@Success		200	{array}		orange.ProjectFilter	"Successful response"
//	@Failure		400	{object}	orange.ErrorResponse
//	@Failure		404	{object}	orange.ErrorResponse
//	@Failure		500	{object}	orange.ErrorResponse
//	@Router			/projects/{id} [get]
func (s *Server) HandleFindProjectById(w http.ResponseWriter, r *http.Request) {
	// Extract ID from URL path
	idStr := r.PathValue("id")

	// Convert id string to uint
	id, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		http.Error(w, "Invalid ID", http.StatusBadRequest)
		return
	}
	ctx := r.Context()
	// Call the FindProjectById method from the ProjectService in the PostgreSQL package
	project, err := s.ProjectService.FindProjectById(ctx, uint(id))
	if err != nil {
		orange.NewError(w, http.StatusInternalServerError, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	// Send JSON response
	json.NewEncoder(w).Encode(project)
}

// handleUpdateProject UpdateProject
//
//	@Summary		Update an  Projects
//	@Description	Update by json projects
//	@Tags			projects
//	@Accept			json
//	@Produce		json
//	@Param			id		path		int				true	"Project ID"
//	@Param			project	body		orange.Project	true	"Updated Project "
//	@Success		200		{object}	orange.Project	"Successful response"
//	@Failure		400		{object}	orange.ErrorResponse
//	@Failure		404		{object}	orange.ErrorResponse
//	@Failure		500		{object}	orange.ErrorResponse
//	@Router			/projects/{id} [put]
func (s *Server) HandleUpdateProject(w http.ResponseWriter, r *http.Request) {
	// Extract project ID from the request URL
	// Extract project ID from the request URL
	parts := strings.Split(r.URL.Path, "/")
	if len(parts) < 3 {
		orange.NewError(w, http.StatusBadRequest, errors.New("invalid URL path"))
		return
	}
	idStr := parts[len(parts)-1] // Get the last part of the URL path
	id, err := strconv.ParseUint(idStr, 10, 32)
	if err != nil {
		orange.NewError(w, http.StatusBadRequest, err)
		return
	}

	// Decode the JSON request body into a Project struct
	var project orange.Project
	err = json.NewDecoder(r.Body).Decode(&project)
	if err != nil {
		orange.NewError(w, http.StatusBadRequest, err)
		return
	}
	validationErrors := validateStruct(&project)
	if validationErrors != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(validationErrors)
		return
	}

	// Call UpdateProject method of ProjectService to update the project in the database
	ctx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
	defer cancel()
	updatedProject, err := s.ProjectService.UpdateProject(ctx, r.Context().Value("userID").(int), uint(id), &project)
	if err != nil {
		orange.NewError(w, http.StatusUnauthorized, err)
		return
	}

	// Respond with success status and updated project
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(updatedProject)
	if err != nil {
		orange.NewError(w, http.StatusInternalServerError, err)
	}
}

// handleDeleteProject DeleteProject
//
//	@Summary		Delete an  Projects
//	@Description	Delete by  projects ID
//	@Tags			projects
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int	true	"Project ID"	Format(uint)
//	@Success		200	{object}	orange.Project
//	@Failure		400	{object}	orange.ErrorResponse
//	@Failure		404	{object}	orange.ErrorResponse
//	@Failure		500	{object}	orange.ErrorResponse
//	@Router			/projects/{id} [delete]
func (s *Server) HandleDeleteProject(w http.ResponseWriter, r *http.Request) {
	// Extract id from request parameters
	idStr := r.PathValue("id")

	// Convert id string to uint
	id, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		http.Error(w, "Invalid ID", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
	defer cancel()

	// Delete project
	err = s.ProjectService.DeleteProject(ctx, r.Context().Value("userID").(int), uint(id))
	if err != nil {
		orange.NewError(w, http.StatusUnauthorized, err)
		return
	}
	if err != nil {
		if errors.Is(err, context.DeadlineExceeded) {
			http.Error(w, "Request timeout", http.StatusRequestTimeout)
			return
		}
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with success
	w.WriteHeader(http.StatusOK)
}
