package http

import (
	"encoding/json"
	"fmt"

	"net/http"

	"gitlab.com/semiconware/orange"
)

func (s *Server) userRegisterRoutes() {

	s.Router.HandleFunc("POST /api/v1/users/register", s.handleRegisterUser)
	s.Router.HandleFunc("POST /api/v1/users/login", s.handleLoginUser)
	s.Router.HandleFunc("POST /api/v1/users/validator", s.validateHandler)
	//	}
}

// handleRegisterUser registers a new user.
//
//	@Summary		Register a new user
//	@Description	Register a new user with the provided details.
//	@Tags			users
//	@Accept			json
//	@Produce		json
//	@Param			user	body		orange.User	true	"User object to register"
//	@Success		200		{object}	orange.User
//	@Failure		400		{object}	orange.ErrorResponse
//	@Failure		404		{object}	orange.ErrorResponse
//	@Failure		500		{object}	orange.ErrorResponse
//	@Router			/users/register [post]
func (s *Server) handleRegisterUser(w http.ResponseWriter, r *http.Request) {
	var user orange.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	if s.DB == nil {
		http.Error(w, "Database connection not available", http.StatusInternalServerError)
		return
	}

	err := s.UserService.RegisterUser(&user) // Pass &project instead of &(&project)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(user)
}

// handleLoginUser handles user login.
//
//	@Summary		Login a user
//	@Description	Logs in a user with the provided credentials.
//	@Tags			users
//	@Accept			json
//	@Produce		json
//	@Param			user	body		orange.User	true	"User credentials for login"
//	@Success		200		{object}	orange.User
//	@Failure		400		{object}	orange.ErrorResponse
//	@Failure		404		{object}	orange.ErrorResponse
//	@Failure		500		{object}	orange.ErrorResponse
//	@Router			/users/login [post]
func (s *Server) handleLoginUser(w http.ResponseWriter, r *http.Request) {
	var user orange.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Login user
	token, err := s.UserService.LoginUser(&user)
	if err != nil {
		http.Error(w, "Invalid email or password", http.StatusUnauthorized)
		return
	}
	w.Header().Set("Authorization", "Bearer"+token)
	w.Header().Set("Content-Type", "application/json")

	// Login successful, return token
	// w.Header().Set("Content-Type", "application/json")
	// w.Header().Set("Authorization", "Bearer"+token)
	// w.WriteHeader(http.StatusOK)
	// json.NewEncoder(w).Encode(map[string]string{"token": token})
	response := map[string]string{
		"token": token,
	}
	if err := json.NewEncoder(w).Encode(response); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (s *Server) validateHandler(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get("Authorization")
	if token == "" {
		http.Error(w, "Authorization token required", http.StatusUnauthorized)
		return
	}
	userID, err := s.UserService.ValidateToken(token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
	}
	w.Write([]byte(fmt.Sprintf("USer ID:%d", userID)))
	w.WriteHeader(http.StatusOK)
}
