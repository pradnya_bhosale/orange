package http
 
import (
    "fmt"
    "reflect"
 
    "github.com/go-playground/validator/v10"
)
 
var validate *validator.Validate
 
func init() {
    validate = validator.New()
}
 
type ValidationErrorResponse struct {
    Errors map[string][]string `json:"errors"`
}
 
func getJSONFieldName(structType reflect.Type, fieldName string) string {
    field, found := structType.FieldByName(fieldName)
    if !found {
        return fieldName
    }
    jsonTag := field.Tag.Get("json")
    if jsonTag == "" || jsonTag == "-" {
        return fieldName
    }
    return jsonTag
}
 
func validateStruct(data interface{}) *ValidationErrorResponse {
    err := validate.Struct(data)
    if err != nil {
        validationErrors := err.(validator.ValidationErrors)
        errorResponse := &ValidationErrorResponse{Errors: make(map[string][]string)}
 
        dataType := reflect.TypeOf(data).Elem()
 
        for _, validationError := range validationErrors {
            fieldName := getJSONFieldName(dataType, validationError.Field())
            message := fmt.Sprintf("Field validation for '%s' failed on the '%s' tag", fieldName, validationError.Tag())
            errorResponse.Errors[fieldName] = append(errorResponse.Errors[fieldName], message)
        }
        return errorResponse
    }
    return nil
}