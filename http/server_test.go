package http_test

import (
	"testing"

	"github.com/jmoiron/sqlx"
	orangehttp "gitlab.com/semiconware/orange/http"
	"gitlab.com/semiconware/orange/mock"
)

type Server struct {
	*orangehttp.Server

	ProjectService mock.ProjectService
}

func MustOpenServer(tb testing.TB) *Server {
	tb.Helper()

	var db *sqlx.DB
	s := &Server{Server: orangehttp.NewServer(db)}

	// Assign mocks to actual server's services.

	//s.Server.ProjectService = &s.ProjectService // Replace mock with your implementation

	srv, err := s.Start()
	if err != nil {
		tb.Fatalf("failed to pen server:%v", err)
	}
	defer srv.Close() //ShoutDown the Server when  the test is complete
	return s
}

func MustCloseServer(tb testing.TB, s *Server) {
	tb.Helper()
	if err := s.Server.Close(); err != nil {
		tb.Fatalf("failed to close server: %v", err)
	}
}
