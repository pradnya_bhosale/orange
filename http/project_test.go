package http_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"

	"gitlab.com/semiconware/orange"
	orangehttp "gitlab.com/semiconware/orange/http"
	"gitlab.com/semiconware/orange/middleware"
	"gitlab.com/semiconware/orange/mock"
)

func TestHandleProjects(t *testing.T) {
	// Start the mocked HTTP test server
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Define mock projects data
	mockProjects := []*orange.Project{
		{
			ID:          1,
			Title:       "Project 1",
			CompletedAt: time.Time{},
			CreatedAt:   time.Time{},
			UpdatedAt:   time.Time{},
			DeletedAt:   time.Time{},
			UserID:      4,
		},
		{
			ID:          2,
			Title:       "Project 2",
			CompletedAt: time.Time{},
			CreatedAt:   time.Time{},
			UpdatedAt:   time.Time{},
			DeletedAt:   time.Time{},
			UserID:      4,
		},
	}

	// Mock the FindProjects method
	s.ProjectService.FindProjectFn = func(ctx context.Context, filter orange.ProjectFilter) ([]*orange.Project, int, error) {
		return mockProjects, len(mockProjects), nil
	}

	// Create a new HTTP request
	req, err := http.NewRequest("GET", "/projects", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Simulate authentication by setting a valid token
	validToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0LCJleHAiOjE3MTg3MDEyMjh9.9uip41GGluQhyQW01lPsY4mWCn8QAlv6VWQGxA0Y-9g"
	req.Header.Set("Authorization", "Bearer "+validToken)

	// Create a mock user service
	mockUserService := &mock.UserService{
		ValidateTokenFn: func(token string) (int, error) {
			// Simulate successful token validation
			return 4, nil
		},
	}

	// Create a mock app instance
	mockApp := &orangehttp.Server{
		ProjectService: &s.ProjectService,
		UserService:    mockUserService,
	}

	// Perform the request
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mockApp.HandleFindProject)
	authHandler := middleware.AuthMiddleware(mockUserService)(handler)
	authHandler.ServeHTTP(rr, req)

	// Check the status code is what we expect
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	// Define a struct to match the response structure
	var response struct {
		Projects   []*orange.Project `json:"projects"`
		TotalCount int               `json:"totalCount"`
		Limit      uint              `json:"limit"`
		Offset     uint              `json:"offset"`
	}

	// Decode the response body
	if err := json.NewDecoder(rr.Body).Decode(&response); err != nil {
		t.Fatal(err)
	}

	// Compare the decoded projects with the mock projects
	if !reflect.DeepEqual(response.Projects, mockProjects) {
		t.Errorf("projects mismatch:\ngot: %#v\nwant: %#v", response.Projects, mockProjects)
	}

	// Check other response fields
	if response.TotalCount != len(mockProjects) {
		t.Errorf("totalCount mismatch: got %d, want %d", response.TotalCount, len(mockProjects))
	}

	if response.Limit != 10 { // Default limit in the handler
		t.Errorf("limit mismatch: got %d, want %d", response.Limit, 10)
	}

	if response.Offset != 0 { // Default offset in the handler
		t.Errorf("offset mismatch: got %d, want %d", response.Offset, 0)
	}
}

func TestHandleCreateProject(t *testing.T) {
	// Start the mocked HTTP test server (assuming `MustOpenServer` and `MustCloseServer` handle server setup and teardown)
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Create a sample project
	project := &orange.Project{
		Title: "NewFlowerProject",
	}

	// Mock the CreateProject method
	var createdProject = &orange.Project{}
	mockProjectService := &mock.ProjectService{
		CreateProjectFn: func(ctx context.Context, userID int, p *orange.Project) error {
			// Simulate successful project creation
			createdProject = &orange.Project{
				ID:          1,
				Title:       p.Title,
				CompletedAt: time.Time{},
				CreatedAt:   time.Time{},
				UpdatedAt:   time.Time{},
				DeletedAt:   time.Time{},
				UserID:      userID,
			}
			*p = *createdProject
			return nil
		},
	}
	// Create the request body
	reqBody, err := json.Marshal(project)
	if err != nil {
		t.Fatal(err)
	}

	// Create a new HTTP request
	req, err := http.NewRequest("POST", "/api/v1/projects", bytes.NewBuffer(reqBody))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	// Set a valid authorization token (mocked token)
	validToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE3MTg3MjgwNTN9.J6qFyhjm15ygUcec9cEVStpvxGQa2Z0wq_okoJYjm1U"
	req.Header.Set("Authorization", "Bearer "+validToken)

	// Create a mock user service
	mockUserService := &mock.UserService{
		ValidateTokenFn: func(token string) (int, error) {
			// Simulate successful token validation
			return 4, nil
		},
	}

	// Create a mock app instance
	mockApp := &orangehttp.Server{
		ProjectService: mockProjectService, // Use the mocked ProjectService
		UserService:    mockUserService,
	}

	// Perform the request
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mockApp.HandleCreateProjects)
	authHandler := middleware.AuthMiddleware(mockUserService)(handler)
	authHandler.ServeHTTP(rr, req)

	// Decode the response body into a struct for comparison
	var got orange.Project
	if err := json.Unmarshal(rr.Body.Bytes(), &got); err != nil {
		t.Fatalf("error decoding response: %v", err)
	}

	// Check if createdProject is nil
	if createdProject == nil {
		t.Fatal("createdProject is nil")
	}

	// Create the expected project
	expected := *createdProject

	// Compare the decoded struct directly
	if !reflect.DeepEqual(got, expected) {
		t.Errorf("response mismatch:\ngot: %#v\nwant: %#v", got, expected)
	}
}
func TestHandleUpdateProject(t *testing.T) {
	// Start the mocked HTTP test server
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Define the original project
	originalProject := &orange.Project{
		ID:          1,
		Title:       "Original Project",
		CompletedAt: time.Time{},
		CreatedAt:   time.Time{},
		UpdatedAt:   time.Time{},
		DeletedAt:   time.Time{},
		UserID:      4,
	}

	// Define the updated project data with valid completedAt and deletedAt
	updatedProject := &orange.Project{
		ID:          1,
		Title:       "Updated Project",
		CompletedAt: time.Now(),  // Provide valid completedAt
		DeletedAt:   time.Time{}, // Provide valid deletedAt
		CreatedAt:   originalProject.CreatedAt,
		UpdatedAt:   time.Now(),
		UserID:      originalProject.UserID,
	}

	// Mock the FindProjectByID method
	s.ProjectService.FindProjectByIdFn = func(ctx context.Context, id uint) (*orange.Project, error) {
		if id == originalProject.ID {
			return originalProject, nil
		}
		return nil, errors.New("project not found")
	}

	// Mock the UpdateProject method
	s.ProjectService.UpdateProjectFn = func(ctx context.Context, userID int, id uint, p *orange.Project) (*orange.Project, error) {
		if id == originalProject.ID {
			originalProject.Title = p.Title
			originalProject.UpdatedAt = time.Now()
			originalProject.CompletedAt = p.CompletedAt // Update completedAt
			originalProject.DeletedAt = p.DeletedAt     // Update deletedAt
			return originalProject, nil
		}
		return nil, errors.New("project not found")
	}

	// Create the request body
	reqBody, err := json.Marshal(updatedProject)
	if err != nil {
		t.Fatal(err)
	}

	// Create a new HTTP request
	req, err := http.NewRequest("PUT", "/projects/1", bytes.NewBuffer(reqBody))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	// Simulate authentication by setting a valid token
	validToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0LCJleHAiOjE3MTg3MDEyMjh9.9uip41GGluQhyQW01lPsY4mWCn8QAlv6VWQGxA0Y-9g"
	req.Header.Set("Authorization", "Bearer "+validToken)

	// Create a mock user service
	mockUserService := &mock.UserService{
		ValidateTokenFn: func(token string) (int, error) {
			// Simulate successful token validation
			return 4, nil
		},
	}

	// Create a mock app instance
	mockApp := &orangehttp.Server{
		ProjectService: &s.ProjectService,
		UserService:    mockUserService,
	}

	// Perform the request
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mockApp.HandleUpdateProject)
	authHandler := middleware.AuthMiddleware(mockUserService)(handler)
	authHandler.ServeHTTP(rr, req)

	// Check the status code is what we expect
	if status := rr.Code; status != http.StatusBadRequest {
		t.Fatalf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}

	// Decode the response body into a struct for comparison
	var response map[string]interface{}
	if err := json.Unmarshal(rr.Body.Bytes(), &response); err != nil {
		t.Fatalf("error decoding response: %v", err)
	}

	// Check the presence of validation errors in the response body
	if _, ok := response["errors"]; !ok {
		t.Error("expected validation errors in response body")
	}

	// Output the response body for more detailed inspection if needed
	t.Logf("response body: %s", rr.Body.String())
}

func TestHandleDeleteProject(t *testing.T) {
	// Start the mocked HTTP test server
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Define the original project
	originalProject := &orange.Project{
		ID:          1,
		Title:       "Project to be deleted",
		CompletedAt: time.Time{},
		CreatedAt:   time.Time{},
		UpdatedAt:   time.Time{},
		DeletedAt:   time.Time{},
		UserID:      4,
	}

	// Mock the FindProjectByID method
	s.ProjectService.FindProjectByIdFn = func(ctx context.Context, id uint) (*orange.Project, error) {
		if id == originalProject.ID {
			return originalProject, nil
		}
		return nil, errors.New("project not found")
	}

	// Mock the DeleteProject method
	s.ProjectService.DeleteProjectFn = func(ctx context.Context, userID int, id uint) error {
		if id == originalProject.ID {
			// Simulate project deletion
			originalProject.DeletedAt = time.Now()
			return nil
		}
		return errors.New("project not found")
	}

	// Create a new HTTP request
	req, err := http.NewRequest("DELETE", "/projects/1", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Simulate authentication by setting a valid token
	validToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0LCJleHAiOjE3MTg3MDEyMjh9.9uip41GGluQhyQW01lPsY4mWCn8QAlv6VWQGxA0Y-9g"
	req.Header.Set("Authorization", "Bearer "+validToken)

	// Create a mock user service
	mockUserService := &mock.UserService{
		ValidateTokenFn: func(token string) (int, error) {
			// Simulate successful token validation
			return 4, nil
		},
	}

	// Create a mock app instance
	mockApp := &orangehttp.Server{
		ProjectService: &s.ProjectService,
		UserService:    mockUserService,
	}

	// Perform the request
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mockApp.HandleDeleteProject)
	authHandler := middleware.AuthMiddleware(mockUserService)(handler)
	authHandler.ServeHTTP(rr, req)

}

func TestHandleFindProjectById(t *testing.T) {
	// Start the mocked HTTP test server
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Define a mock project
	mockProject := &orange.Project{
		ID:          1,
		Title:       "Mock Project",
		CompletedAt: time.Time{},
		CreatedAt:   time.Time{},
		UpdatedAt:   time.Time{},
		DeletedAt:   time.Time{},
		UserID:      4,
	}

	// Mock the FindProjectById method
	s.ProjectService.FindProjectByIdFn = func(ctx context.Context, id uint) (*orange.Project, error) {
		if id == mockProject.ID {
			return mockProject, nil
		}
		return nil, errors.New("project not found")
	}

	// Create a new HTTP request
	req, err := http.NewRequest("GET", "/projects/1", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Simulate authentication by setting a valid token
	validToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0LCJleHAiOjE3MTg3MDEyMjh9.9uip41GGluQhyQW01lPsY4mWCn8QAlv6VWQGxA0Y-9g"
	req.Header.Set("Authorization", "Bearer "+validToken)

	// Create a mock user service
	mockUserService := &mock.UserService{
		ValidateTokenFn: func(token string) (int, error) {
			// Simulate successful token validation
			return 4, nil
		},
	}

	// Create a mock app instance
	mockApp := &orangehttp.Server{
		ProjectService: &s.ProjectService,
		UserService:    mockUserService,
	}

	// Perform the request
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mockApp.HandleFindProjectById)
	authHandler := middleware.AuthMiddleware(mockUserService)(handler)
	authHandler.ServeHTTP(rr, req)

	// Check the status code is what we expect
	if status := rr.Code; status != http.StatusBadRequest {
		t.Fatalf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}

	// Decode the response body into a struct for comparison
	// var response map[string]interface{}
	// if err := json.Unmarshal(rr.Body.Bytes(), &response); err != nil {
	// 	t.Fatalf("error decoding response: %v", err)
	// }
	// Compare the decoded project with the mock project
	// if !reflect.DeepEqual(response.Project, mockProject) {
	// 	t.Errorf("project mismatch:\ngot: %#v\nwant: %#v", response.Project, mockProject)
	// }
}
