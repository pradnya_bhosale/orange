package http

import (
	"context"
	"fmt"
	"log"
	"time"

	"net/http"

	"github.com/jmoiron/sqlx"
	httpSwagger "github.com/swaggo/http-swagger"

	"gitlab.com/semiconware/orange"
	"gitlab.com/semiconware/orange/middleware"
)

type Server struct {
	Http   *http.Server
	Router *http.ServeMux
	DB     *sqlx.DB

	// If domain is specified, server is run on TLS using acme/autocert.
	Addr string
	//projectService *postgres.ProjectService
	UserService    orange.UserService
	ProjectService orange.ProjectService
	Handler        http.Handler
}

func NewServer(db *sqlx.DB) *Server {
	// db := postgres.InitDB()

	sm := http.NewServeMux()
	ss := http.NewServeMux()

	ss.Handle("/api/v1/", http.StripPrefix("/api/v1", sm))
	//ss.Handle("/", middleware.CROS(http.HandlerFunc(handleOther)))
	//handler := middleware.CROS(ss)

	ss.Handle(
		"/swagger/*",
		httpSwagger.Handler(httpSwagger.URL("http://localhost:8585/swagger/doc.json")),
	)
	loggedRouter := middleware.Logging(ss)
	// authMiddleware := middleware.AuthMiddleware(loggedRouter)
	corsRouter := middleware.CROS(loggedRouter)

	s := &Server{
		Http: &http.Server{
			//Handler: ss,
			Handler: corsRouter,
		},
		Router: ss,
		DB:     db,
	}

	return s
}

func (s *Server) Start() (*http.Server, error) {
	s.projectRoutes()
	s.userRegisterRoutes()

	srv := &http.Server{
		Addr:    ":8585",
		Handler: s.Http.Handler,
		//Handler: middleware.CROS(s.Router),
	}
	go func() {

		fmt.Println("create server")
		log.Println("Starting server on :8585")

		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal("Server error:", err)
		}
	}()
	return srv, nil
}

// ShutdownTimeout is the time given for outstanding requests to finish before shutdown.
const ShutdownTimeout = 1 * time.Second

// Close gracefully shuts down the server.
func (s *Server) Close() error {
	// Create a context with a timeout to gracefully shut down the server.
	ctx, cancel := context.WithTimeout(context.Background(), ShutdownTimeout)
	defer cancel()

	return s.Http.Shutdown(ctx)
}
