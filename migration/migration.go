package migration

import (
	migratemigrate "github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jmoiron/sqlx"
)

func ApplyMigrations(db *sqlx.DB, dbName, user, password string) error {
	migrationsDB, err := sqlx.Open("pgx", "host=localhost user=b_pradnya  password=Knight@123 dbname=migratedb port=5432")

	if err != nil {
		return err
	}
	defer migrationsDB.Close()

	driver, err := postgres.WithInstance(migrationsDB.DB, &postgres.Config{})
	if err != nil {
		return err
	}

	sourceInstance, err := (&file.File{}).Open("file://migration")
	if err != nil {
		return err
	}

	m, err := migratemigrate.NewWithInstance("file", sourceInstance, "postgres", driver)
	if err != nil {
		return err
	}

	if err := m.Up(); err != nil && err != migratemigrate.ErrNoChange {
		return err
	}

	return nil
}
