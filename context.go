package orange

import "context"

// interfering with our context keys.
type contextKey int

const (
	// Stores the current logged in user in the context.
	userContextKey = contextKey(iota + 1)
)

// UserFromContext returns the current logged in user.
func UserFromContext(ctx context.Context) *Project {
	user, _ := ctx.Value(userContextKey).(*Project)
	return user
}
