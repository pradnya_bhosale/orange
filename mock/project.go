package mock

import (
	"context"

	"gitlab.com/semiconware/orange"
)

var _ orange.ProjectService = (*ProjectService)(nil)

// ProjectService represents a mock of .ProjectService.
type ProjectService struct {
	FindProjectByIdFn func(ctx context.Context, id uint) (*orange.Project, error)
	FindProjectFn     func(ctx context.Context, filter orange.ProjectFilter) ([]*orange.Project, int, error)
	//FindProjectFn   func(project *[]orange.Project) error

	CreateProjectFn func(ctx context.Context, userID int, project *orange.Project) error
	UpdateProjectFn func(ctx context.Context, userID int, id uint, project *orange.Project) (*orange.Project, error)
	DeleteProjectFn func(ctx context.Context, userID int, id uint) error
}

func (s *ProjectService) FindProjectById(ctx context.Context, id uint) (*orange.Project, error) {
	return s.FindProjectByIdFn(ctx, id)
}

// func (s *ProjectService) FindProject(project orange.Project) ([]*orange.Project, uint, error) {
// 	return s.FindProjectFn(project)
// }

func (s *ProjectService) FindProject(ctx context.Context, filter orange.ProjectFilter) ([]*orange.Project, int, error) {
	return s.FindProjectFn(ctx, filter)
}

// CreateProject mocks the CreateProject method of orange.ProjectService.
func (s *ProjectService) CreateProject(ctx context.Context, userID int, project *orange.Project) error {
	return s.CreateProjectFn(ctx, userID, project)
}

func (s *ProjectService) UpdateProject(ctx context.Context, userID int, id uint, project *orange.Project) (*orange.Project, error) {
	return s.UpdateProjectFn(ctx, userID, id, project)
}

func (s *ProjectService) DeleteProject(ctx context.Context, userID int, id uint) error {
	return s.DeleteProjectFn(ctx, userID, id)
}
