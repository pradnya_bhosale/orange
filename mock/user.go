package mock

import "gitlab.com/semiconware/orange"

var _ orange.UserService = (*UserService)(nil)

type UserService struct {
	RegisterUserFn func(user *orange.User) error
	//LoginUser(user *User) error
	LoginUserFn     func(username, password string) (string, error)
	ValidateTokenFn func(token string) (int, error)
}

func (s *UserService) RegisterUser(user *orange.User) error {
	return s.RegisterUserFn(user)
}

func (s *UserService) LoginUser(user *orange.User) (string, error) {
	return s.LoginUserFn(user.Username, user.Password)
}
func (s *UserService) ValidateToken(token string) (int, error) {
	return s.ValidateTokenFn(token)
}
