package orange

import (

	//"fmt"

	"time"

	_ "github.com/jackc/pgx/v5/stdlib"
	"golang.org/x/crypto/bcrypt"

	"github.com/jmoiron/sqlx"
)

type User struct {
	ID        int       `json:"id" db:"id"`
	FullName  string    `json:"fullname" db:"fullname"`
	Email     string    `json:"email" db:"email"`
	Username  string    `json:"username" db:"username"`
	Password  string    `json:"password" db:"password"`
	IsActive  bool      `json:"isactive" db:"isactive"`
	CreatedAt time.Time `json:"createdAt" db:"created_at"`
	UpdatedAt time.Time `json:"updatedAt" db:"updated_at"`
	DeletedAt time.Time `json:"deletedAt" db:"deleted_at"`
}

func CreateUserTable(db *sqlx.DB) error {
	_, err := db.Exec(`
        CREATE TABLE IF NOT EXISTS users (
            id SERIAL PRIMARY KEY,
            fullname VARCHAR(255),
			email VARCHAR(255),
			username  VARCHAR(255),
			password VARCHAR(255),
			isactive BOOLEAN,
            created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            updated_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            deleted_at TIMESTAMP
        )
    `)
	return err
}

type UserFilter struct {
	ID       int    `json:"id" db:"id"`
	FullName string `json:"fullname" db:"fullname"`
	Email    string `json:"email" db:"email"`
	IsActive bool   `json:"isactive" db:"isactive"`
}
type UserService interface {
	RegisterUser(user *User) error
	//LoginUser(user *User) error
	LoginUser(user *User) (string, error)
	ValidateToken(token string) (int, error)
}

const (
	MinCost     int = 4  // the minimum allowable cost as passed in to GenerateFromPassword
	MaxCost     int = 31 // the maximum allowable cost as passed in to GenerateFromPassword
	DefaultCost int = 10 // the cost that will actually be set if a cost below MinCost is passed into GenerateFromPassword
)

// HashPassword hashes the user's password using bcrypt
func (u *User) HashPassword() error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)
	// Replace the hashed password with asterisks
	//u.Password = strings.Repeat("*", len(hashedPassword))
	return nil
}

// CheckPassword checks if the provided password matches the hashed password

func (u *User) CheckPassword(providedPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(providedPassword))
	return err == nil
}
