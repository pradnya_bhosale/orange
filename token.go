package orange

type Token struct {
	ID     uint   `json:"id" db:"id"`
	UserID uint   `json:"id" user_db:"user_id"`
	Token  string `json:"token" db:"token"`
}
