package main

import (
	"log"
	"os"
	"os/signal"

	_ "github.com/jackc/pgx/v5"
	"gitlab.com/semiconware/orange"
	_ "gitlab.com/semiconware/orange/docs"
	"gitlab.com/semiconware/orange/http"
	"gitlab.com/semiconware/orange/migration"
	"gitlab.com/semiconware/orange/postgres"
)

// @title			Orange
// @version		1.0
// @description	This is a sample server Petstore server.
// @termsOfService	http://swagger.io/terms/
// @contact.name	API Support
// @contact.url	http://www.orange.io/support
// @contact.email	support@orange.io
// @license.name	Apache 2.0
// @license.url	http://www.apache.org/licenses/LICENSE-2.0.html
// @host			localhost:8585
// @BasePath		/api/v1
func main() {
	db := postgres.InitDB()
	//postgres.TestDB()
	//projecttable()
	//Usertable()

	if err := orange.CreateUserTable(db); err != nil {
		defer db.Close()
		log.Fatal("Error creating project table:", err)
	}
	if err := orange.CreateProjectTable(db); err != nil {
		defer db.Close()
		log.Fatal("Error creating project table:", err)
	}
	err := migration.ApplyMigrations(db, "migratedb", "b_pradnya", "Knight@123")
	if err != nil {
		log.Fatalf("Error applying migrations: %v", err)
	}
	server := http.NewServer(db)
	projectService := postgres.NewProjectService(db)
	server.ProjectService = projectService
	userService := postgres.NewUserService(db)
	server.UserService = userService

	srv, err := server.Start()

	if err != nil {
		log.Fatalf("error opening Server:%v", err)
	}
	defer srv.Close()

	log.Printf("server is listening on %s", ":8585")
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("shutting down server...")

}
