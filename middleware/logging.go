package middleware

import (
	"log/slog"
	"net/http"
	"os"
	"time"
)

var (
	logger       *slog.Logger
	debugLogging bool // Flag to control debug logging
)

func init() {
	logFile, err := os.OpenFile("app.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	handler := slog.NewJSONHandler(logFile, &slog.HandlerOptions{})
	logger = slog.New(handler)

	// Set debugLogging to true or false based on your application's configuration
	debugLogging = true // or false based on your requirement
}

type wrappedWriter struct {
	http.ResponseWriter
	statusCode int
}

func (w *wrappedWriter) WriteHeader(statusCode int) {
	if w.statusCode == 0 { // Check if WriteHeader was already called
		w.statusCode = statusCode
		w.ResponseWriter.WriteHeader(statusCode)
	}
}
func Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		wrapped := &wrappedWriter{
			ResponseWriter: w,
			statusCode:     http.StatusOK,
		}

		next.ServeHTTP(wrapped, r)
		duration := time.Since(start)

		// Log request processing details
		logger.Info("Request processed",
			slog.Int("status_code", wrapped.statusCode),
			slog.String("method", r.Method),
			slog.String("path", r.URL.Path),
			slog.Duration("duration", duration),
		)

		// Add debug log
		if debugLogging {
			logger.Info("Debugging")
		}
	})
}
