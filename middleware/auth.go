package middleware

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/semiconware/orange"
)

func AuthMiddleware(userservice orange.UserService) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			token := r.Header.Get("Authorization")
			if token == "" {
				http.Error(w, "Authorization token required", http.StatusUnauthorized)
				return
			}
			tokenString := strings.TrimPrefix(token, "Bearer ")

			// Validate the JWT token
			userID, err := userservice.ValidateToken(tokenString)

			fmt.Printf("Validated token successfully. User ID: %d\n", userID)
			if err != nil {
				http.Error(w, "invalid or expired token", http.StatusUnauthorized)
				return
			}

			// Set user ID in request context
			ctx := context.WithValue(r.Context(), "userID", userID)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
