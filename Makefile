run:
	go run cmd/oranged/main.go


.PHONY: update-docs
update-docs: ## Update swagger api docs generated using swag
	@swag fmt
	@swag init -g cmd/oranged/main.go

.PHONY: setup
setup: ## Install required tools
	@command -v air 2>&1 >/dev/null || go install github.com/cosmtrek/air@latest
	@command -v swag 2>&1 >/dev/null || go install github.com/swaggo/swag/cmd/swag@latest

